import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Typography from "../atoms/Typography";

interface Props {
  onChange: (event: React.ChangeEvent<HTMLSelectElement>) => void;
  value: string | number;
  name?: string;
  className?: string;
  title?: string;
  placeholder?: string;
  children?: React.ReactElement | React.ReactElement[];
}

function SelectInput(props: Props) {
  return (
    <div className={props.className}>
      <div className={"SelectInputContainer"}>
        <div className="SelectInputTitleContainer">
          <Typography>{props.title}</Typography>
        </div>
        <div className="SelectContainer">
          <select name={props.name} onChange={props.onChange}>
            {props.children}
          </select>
        </div>
      </div>
    </div>
  );
}

const StyledSelectInput = styled(SelectInput)<Props>`
  .SelectInputContainer {
    border-bottom: 1px solid ${(props) => props.theme.colors.text.content};
    max-width: 235px;

    & .SelectInputTitleContainer {
      display: flex;
      flex-direction: row;
      text-align: initial;
      align-items: center;
    }

    & .SelectContainer {
      display: flex;
      flex-grow: 1;
      color: ${(props) => props.theme.colors.text.content};
      border: none;
      &:focus-visible {
        outline: none;
      }
      & select {
        flex-grow: 1;
        border: none;
        margin-top: 15px;
      }
    }
  }
`;

export default StyledSelectInput;

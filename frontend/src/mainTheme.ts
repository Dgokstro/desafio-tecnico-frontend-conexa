import { DefaultTheme } from "styled-components";

const theme = {
  colors: {
    primary: {
      main: "#2E50D4",
      light: "#2E50F4",
      dark: "#2E50B4",
    },
    secondary: {
      main: "#FFFFFB",
      light: "#FFFFFF",
      dark: "#FFFFF0",
    },
    text: {
      title: "#1C307F",
      subtitle: "#575453",
      content: "#999392",
      error: "#ff3333",
    },
    disabled: {
      text: "#FFF",
      background: "lightgray",
    },
  },
};

export default theme;
export type Theme = typeof theme & DefaultTheme;

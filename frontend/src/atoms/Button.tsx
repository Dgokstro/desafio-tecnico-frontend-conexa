import React from "react";
import styled from "styled-components";

interface Props {
  color?: "primary" | "secondary";
  disabled?: boolean;
}

const Button = styled.button<Props>`
  border-radius: 1em;
  padding: 1em;
  cursor: pointer;
  size: 14px;
  color: ${(props) =>
    props.color && props.color === "secondary"
      ? props.theme.colors.primary.main
      : props.theme.colors.secondary.main};

  background-color: ${(props) =>
    props.color && props.color === "secondary"
      ? props.theme.colors.secondary.main
      : props.theme.colors.primary.main};
  border: ${(props) =>
    props.color && props.color === "secondary"
      ? `2px solid ${props.theme.colors.primary.main}`
      : `2px solid ${props.theme.colors.primary.main}`};

  &:hover {
    background-color: ${(props) =>
      props.color && props.color === "secondary"
        ? props.theme.colors.secondary.light
        : props.theme.colors.primary.light};
    box-shadow: 3px 3px 5px grey;
  }
  ${(props) =>
    props.disabled
      ? `color:${props.theme.colors.disabled.text};  
      background-color:${props.theme.colors.disabled.background};  
      border:${props.theme.colors.disabled.background};
      &:hover {
        background-color:${props.theme.colors.disabled.background};  
        box-shadow: none;
      } `
      : ""}
`;

export default Button;

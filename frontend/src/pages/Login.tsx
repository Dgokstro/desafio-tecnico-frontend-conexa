import React, { useEffect, useState } from "react";
import axios from "axios";
import Typography from "../atoms/Typography";
import StyledTopBar from "../molecules/TopBar";
import loginImg from "../assets/login.png";
import StyledTextInput from "../molecules/TextInput";
import * as yup from "yup";
import { useFormik } from "formik";
import Button from "../atoms/Button";
import styled from "styled-components";
import { BrowserRouter, Redirect, useHistory } from "react-router-dom";

const validationSchema = yup.object({
  email: yup.string().email("Email invalido").required("Digite seu email"),
  password: yup.string().required("Digite sua senha"),
});

interface Props {
  className?: string;
}

const OverflowTypography = styled(Typography)`
  font-family: "Montserrat", sans-serif;
  font-size: 52px;
`;

function Login(props: Props) {
  const [error, setError] = useState("");
  const [token, setToken] = useState(
    Boolean(window.localStorage.getItem("token"))
  );
  const history = useHistory();
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema,
    onSubmit(values, { setSubmitting }) {
      axios({
        method: "POST",
        url: "http://localhost:3333/login",
        data: values,
      })
        .then((response) => {
          window.localStorage.setItem("token", response.data.token);
          window.localStorage.setItem("name", response.data.name);
          setSubmitting(false);
          history.push("/home");
        })
        .catch((error) => {
          setError(error?.response?.data.message);
          setSubmitting(false);
        });
    },
  });

  useEffect(() => {
    formik.validateForm();
  }, []);

  return token ? (
    <Redirect to="/home" />
  ) : (
    <div className={props.className}>
      <StyledTopBar />
      <div className="contentContainer">
        <div className="titleContainer">
          <OverflowTypography type="title">Faça Login</OverflowTypography>
          <img src={loginImg} alt="Login" className="loginImg" />
        </div>
        <form onSubmit={formik.handleSubmit} className="formContainer">
          <StyledTextInput
            name="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            title="E-mail"
            placeholder="Digite seu e-mail"
          />
          <StyledTextInput
            name="password"
            value={formik.values.password}
            onChange={formik.handleChange}
            helper
            title="Senha"
            type="password"
            placeholder="Digite sua senha"
          />
          {error && <Typography type="error">{error}</Typography>}
          <Button
            type="submit"
            disabled={!formik.isValid || formik.isSubmitting}
          >
            Entrar
          </Button>
        </form>
      </div>
    </div>
  );
}

const StyledLogin = styled(Login)`
  display: flex;
  flex-direction: column;
  height: 100vh;
  & .contentContainer {
    display: flex;
    flex-direction: row;
    flex-grow: 1;
    width: 100vw;
    justify-content: space-evenly;
    align-items: center;
    & .titleContainer {
      display: flex;
      flex-direction: column;
      text-align: center;
      height: 100%;
      justify-content: center;
      & :first-child {
        margin-bottom: 50px;
      }
      & .loginImg {
        width: 338px;
        height: 266px;
      }
    }
    & .formContainer {
      width: 235px;
      display: flex;
      flex-direction: column;
      height: 100%;
      justify-content: space-between;
      max-height: 264px;
    }
  }
  @media (max-width: 768px) {
    & .contentContainer {
      flex-direction: column;
      & .titleContainer {
        flex-grow: 0;
        height: inherit;
        & .loginImg {
          display: none;
        }
      }
      & .formContainer {
        width: 100%;
        padding: 0px 15px;
        box-sizing: border-box;
        max-width: 350px;
        & .TextInputContainer {
          max-width: 350px;
        }
      }
    }
  }
`;

export default StyledLogin;

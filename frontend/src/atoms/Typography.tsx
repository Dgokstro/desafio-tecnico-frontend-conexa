import React from "react";
import styled from "styled-components";

type TextType = "title" | "subtitle" | "content" | "error";

interface Props {
  type?: TextType;
  fontSize?: number;
}

const Typography = styled.label<Props>`
  ${(props) => {
    switch (props.type) {
      case "title":
        return `
            color : ${props.theme.colors.text.title};
            font-size: 48px
            `;
      case "subtitle":
        return `
            color : ${props.theme.colors.text.subtitle};
            font-size: 16px
            `;
      case "content":
        return `
            color : ${props.theme.colors.text.content};
            font-size: 16px;
            font-weight: 400;
            `;
      case "error":
        return `
            color : ${props.theme.colors.text.error};
            font-size: 16px;
            font-weight: 400;
            `;
    }
  }}
`;
export default Typography;

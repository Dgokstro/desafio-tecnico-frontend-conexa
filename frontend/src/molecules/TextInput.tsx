import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Typography from "../atoms/Typography";
import helper from "../assets/helper.png";
import visibleIcon from "../assets/visible.png";
import notVisibleIcon from "../assets/notVisible.png";

interface Props {
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  value: string;
  name?: string;
  type?: "text" | "password";
  className?: string;
  title?: string;
  placeholder?: string;
  helper?: boolean;
}

function TextInput(props: Props) {
  const [type, setType] = useState(props.type ?? "text");
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    props.type === "password" && !visible
      ? setType("password")
      : setType("text");
  }, [visible]);

  return (
    <div className={props.className}>
      <div className={"TextInputContainer"}>
        <div className="TextInputTitleContainer">
          <Typography>{props.title}</Typography>
          {props.helper && <img src={helper} alt="helper" />}
        </div>
        <div className="inputContainer">
          <input
            type={type}
            onChange={props.onChange}
            value={props.value}
            name={props.name}
            className="input"
            placeholder={props.placeholder}
          />
          {props.type === "password" && (
            <img
              src={visible ? visibleIcon : notVisibleIcon}
              alt="helper"
              onClick={() => setVisible(!visible)}
            />
          )}
        </div>
      </div>
    </div>
  );
}

const StyledTextInput = styled(TextInput)<Props>`
  .TextInputContainer {
    border-bottom: 1px solid ${(props) => props.theme.colors.text.content};
    max-width: 235px;

    & .TextInputTitleContainer {
      display: flex;
      flex-direction: row;
      text-align: initial;
      align-items: center;
      & img {
        max-width: 15px;
        padding-left: 9px;
        max-height: 15px;
      }
    }

    & .inputContainer {
      display: flex;
      & img {
        width: 18px;
      }
      & input {
        flex-grow: 1;
        color: ${(props) => props.theme.colors.text.content};
        border: none;
        &:focus-visible {
          outline: none;
        }
      }
    }
  }
`;

export default StyledTextInput;

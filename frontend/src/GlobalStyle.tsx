import { createGlobalStyle, ThemeProps } from "styled-components";
import { Theme } from "./mainTheme";

const GlobalStyle = createGlobalStyle`
*{
    color:${(props: ThemeProps<Theme>) => props.theme.colors.text.content};
    font-family: 'Nunito', sans-serif;
    font-weight: 700;
    margin:0px;
}
`;

export default GlobalStyle;

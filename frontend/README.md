# Desafio Conexa

Projeto FrontEnd para poder vizualizar e cadastrar novas consultas.

<!--te-->

## Recursos Necessários

Abaixo, o que você vai precisar para rodar a aplicação no seu computador:

- [Editor de texto](https://code.visualstudio.com/download) (Recomendado: Visual Studio Code)
- [Node.JS](https://nodejs.org) 14+
- [npm](https://www.npmjs.com/get-npm) (Acompanha instalação do Node.js)
- [Yarn](https://classic.yarnpkg.com/en/docs/install/#windows-stable)

## Arquitetura

A aplicação foi construída utilizando a linguagem `JavaScript` e tipado com `TypeScript`,

Já as interfaces, foram construídas utilizando a metodologia [Atomic Design](https://atomicdesign.bradfrost.com/chapter-2), com o `React.js` e componentes baseados no [Figma](https://www.figma.com/file/eaD2LIOcswFJO2SblVyIeq/Desafio-frontend-Conexa?node-id=209%3A1807) utilizando o `Styled-Components`.

Abaixo estão os principais frameworks e bibliotecas do projeto e suas funcionalidades:

| Framework / Biblioteca                              | Versão | Descrição                                                                         |
| --------------------------------------------------- | ------ | --------------------------------------------------------------------------------- |
| [TypeScript](https://typescriptlang.org)            | 4      | Adiciona tipagem e alguns outros recursos a linguagem JavaScript                  |
| [React.js](https://reactjs.org)                     | 17     | Criação de interfaces de usuários web                                             |
| [Axios](https://github.com/axios/axios)             | 0.21.1 | Bliblioteca baseada em Promises para fazer requisições HTTP no Node.js e React.js |
| [Formik](https://formik.org)                        | 2      | Biblioteca para criação de formulários, validação e gerenciamento de estado       |
| [Yup](https://github.com/jquense/yup)               | 0.32.9 | Biblioteca para criação de schemas para validação de formulários                  |
| [Styled-Components](https://styled-components.com/) | 5.3.0  | Biblioteca para a criação de componentes estilizados                              |
| [react-router-dom](https://reactrouter.com/)        | 5.2.0  | Biblioteca para auxilio de rotas                                                  |

## Estrutura dos Arquivos

```
.
├── public (arquivos públicos)
├── src (aplicação)
│   ├── assets(arquivos complementares)
│   ├── atoms(menor parte de um componente)
│   ├── molecules(encapsulamento dos atoms)
│   ├── organisms(encapsulamento das molecules)
│   ├── pages(construção da pagina com toda sua lógica)
│   ├── templates(conjunto de componentes reutilizáveis)
│   ├── pages (rotas das páginas)
│   ├── App.tsx (Inicio da aplicação)
│   ├── GlobaStyles.tsx (Estilo Global independente de componentes)
│   ├── index.tsx (Ponto de entrada do React.js)
│   ├── mainTheme.ts (Temas)
└── tsconfig.json (configurações do TypeScript)
```

## Setup

```bash
# Instalar pacotes de dependências
yarn
```

## Como Utilizar

Após clonar o projeto e instalar os pacotes de dependências, execute o comando:

```bash
yarn start
```

A aplicação iniciará no seguinte endereço: http://localhost:3000.

## TODOs

- [ ] Testes de componentes
  - [ ] instalar storybook
  - [ ] configurar storybook
  - [ ] criar testes de componentes isolados
- [ ] Testes E2E
  - [ ] instalar Cypress
  - [ ] configurar Cypress
  - [ ] desenhar testes
  - [ ] criar testes
- [ ] Melhorar Feedback
  - [ ] implementar retorno de erro
  - [ ] implementar retorno de sucesso
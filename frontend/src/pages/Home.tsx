import React, { useEffect, useState } from "react";
import Button from "../atoms/Button";
import IlustrationPlant from "../assets/illustration-plant.png";
import IlustrationCertificates from "../assets/illustration-certificates.png";
import Typography from "../atoms/Typography";
import styled from "styled-components";
import StyledBasicTemplate from "../templates/BasicTemplate";
import axios, { AxiosResponse } from "axios";

interface Props {
  className?: string;
}
type SchedulerAppointmentsResponse = {
  id: number;
  patientId: number;
  date: Date;
  patient: {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
  };
}[];

function Home(props: Props) {
  const [schedulerAppointments, setSchedulerAppointments] =
    useState<SchedulerAppointmentsResponse>([]);
  const token = window.localStorage.getItem("token");

  useEffect(() => {
    axios("http://localhost:3333/consultations?_expand=patient", {
      method: "GET",
      headers: { authorization: token },
    })
      .then((response: AxiosResponse<SchedulerAppointmentsResponse>) =>
        setSchedulerAppointments(response.data)
      )
      .catch((error) =>
        console.log("Não foi possivel carregar os itens", error)
      );
  }, []);

  return (
    <StyledBasicTemplate title="Consultas">
      <div className={props.className}>
        {schedulerAppointments.length > 0 ? (
          <div className={"appointmentsContainer"}>
            <div className="appointmentSubtitle">
              <Typography type="subtitle">{`${schedulerAppointments.length} ${
                schedulerAppointments.length > 1
                  ? "consultas agendadas"
                  : "consulta agendada"
              } `}</Typography>
            </div>
            {schedulerAppointments.map((appointment) => {
              const appointmentDate = new Date(appointment.date);
              return (
                <div className="appointmentSubtitleItens">
                  <div className="appointmentInfo">
                    <Typography>{`${appointment.patient.first_name} ${appointment.patient.last_name}`}</Typography>
                    <Typography>{`${appointmentDate.getDate()}/${appointmentDate.getMonth()}/${appointmentDate.getFullYear()} às ${String(
                      appointmentDate.getHours()
                    ).padStart(2, "0")} : ${String(
                      appointmentDate.getMinutes()
                    ).padStart(2, "0")}`}</Typography>
                  </div>
                  <Button>Atender</Button>
                </div>
              );
            })}
          </div>
        ) : (
          <>
            <img
              src={IlustrationCertificates}
              alt="Certificates"
              className="certificatesImg"
            />
            <div className="infoBox">
              <Typography type="content">
                Não há nenhuma consulta agendada
              </Typography>
            </div>
            <img src={IlustrationPlant} alt="Plant" className="plantImg" />
          </>
        )}
      </div>
    </StyledBasicTemplate>
  );
}

const StyledHome = styled(Home)`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  max-height: 360px;
  max-width: 520px;
  & .plantImg {
    height: 89px;
    width: 81px;
  }
  & .certificatesImg {
    height: 60px;
    width: 90px;
    align-self: flex-end;
  }
  & .infoBox {
    flex-grow: 1;
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
  }
  & .appointmentsContainer {
    & .appointmentSubtitle {
      margin-bottom: 24px;
    }
    & .appointmentSubtitleItens {
      display: flex;
      flex-direction: row;
      width: 100%;
      justify-content: space-between;
      margin: 32px 0px;

      & .appointmentInfo {
        display: flex;
        flex-direction: column;
        & :last-child {
          font-size: 11px;
        }
      }
    }
  }
`;

export default StyledHome;

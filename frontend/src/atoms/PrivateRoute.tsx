import React from "react";
import { Redirect, Route } from "react-router";

interface Props {
  children: React.ReactElement;
  path: string;
}
export default function PrivateRoute(props: Props) {
  const token = Boolean(window.localStorage.getItem("token"));

  return token ? (
    <Route path={props.path}>{props.children}</Route>
  ) : (
    <Redirect to="/login" />
  );
}
